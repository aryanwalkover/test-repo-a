# Docker Image which is used as foundation to create
# a custom Docker Image with this Dockerfile
FROM node:13.12.0-alpine

# A directory within the virtualized Docker environment
# Becomes more relevant when using Docker Compose later
RUN mkdir -p /test-repo
WORKDIR /test-repo

# Copies package.json and package-lock.json to Docker environment
COPY package*.json ./

# Copies everything over to Docker environment
COPY . /test-repo/

# Uses port which is used by the actual application
EXPOSE 9090
RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent

# add app

CMD serve -s build -p 9090
# Finally runs the application
#CMD [ "serve", "-s", "build" "-p" "6060" ]